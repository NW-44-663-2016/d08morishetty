using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Morishetty.Models;

namespace D08Morishetty.Controllers
{
    [Produces("application/json")]
    [Route("api/EventTs")]
    public class EventTsController : Controller
    {
        private ApplicationDbContext _context;

        public EventTsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/EventTs
        [HttpGet]
        public IEnumerable<EventT> GetEvents()
        {
            return _context.Events;
        }

        // GET: api/EventTs/5
        [HttpGet("{id}", Name = "GetEventT")]
        public IActionResult GetEventT([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            EventT eventT = _context.Events.Single(m => m.EventID == id);

            if (eventT == null)
            {
                return HttpNotFound();
            }

            return Ok(eventT);
        }

        // PUT: api/EventTs/5
        [HttpPut("{id}")]
        public IActionResult PutEventT(int id, [FromBody] EventT eventT)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != eventT.EventID)
            {
                return HttpBadRequest();
            }

            _context.Entry(eventT).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventTExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/EventTs
        [HttpPost]
        public IActionResult PostEventT([FromBody] EventT eventT)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Events.Add(eventT);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EventTExists(eventT.EventID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetEventT", new { id = eventT.EventID }, eventT);
        }

        // DELETE: api/EventTs/5
        [HttpDelete("{id}")]
        public IActionResult DeleteEventT(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            EventT eventT = _context.Events.Single(m => m.EventID == id);
            if (eventT == null)
            {
                return HttpNotFound();
            }

            _context.Events.Remove(eventT);
            _context.SaveChanges();

            return Ok(eventT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventTExists(int id)
        {
            return _context.Events.Count(e => e.EventID == id) > 0;
        }
    }
}